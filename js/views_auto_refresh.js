/**
 * @file
 * JavaScript for refreshing view, enable/disable auto refresh button and store refreshing state behaviour in cookies
 */
/* eslint-disable camelcase */
/* eslint-disable func-names */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */

(function ($, Drupal, drupalSettings, once, cookies) {
  const COOKIE_TRUE = 'true';
  const COOKIE_FALSE = 'false';
  const DATA_HAS_FOCUS = 'hasFocus';
  const DATA_FOCUSED_ELEMENT_ID = 'focusedElementId';
  const PREFIX = 'views_auto_refresh';
  const FOCUS_DEFAULT_ID = `${PREFIX}-default-focus-id`;

  function getViewSelector(view_name, view_display) {
    const view_name_class = view_name.replace(/_/g, '-');
    return `.view-${view_name_class}.view-display-id-${view_display}`;
  }

  function getCookieName(view_name, view_display) {
    return `${PREFIX}-${view_name}-${view_display}`;
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   * @return {string} - The auto-refresh cookie value: 'true' or 'false'.
   */
  function getAutoRefreshCookie(viewData) {
    return cookies.get(viewData.cookieName);
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   * @param {boolean} state - Sets the cookie to 'true' or 'false'.
   */
  function setAutoRefreshCookie(viewData, state) {
    cookies.set(viewData.cookieName, state ? COOKIE_TRUE : COOKIE_FALSE);
  }

  /**
   * Checks the auto-refresh cookie and the auto_start_refresh configuration
   * option to determine if auto-refresh should be enabled or not.
   * @param {ViewData} viewData - The viewData object.
   * @return {boolean} - true if auto-refresh is enabled, false otherwise.
   */
  function isAutoRefreshEnabled(viewData) {
    if (viewData.settings.auto_refresh_toggle_button) {
      const cookie = getAutoRefreshCookie(viewData);
      if (cookie === undefined || cookie === null) {
        const auto_start = viewData.settings.auto_start_refresh;
        setAutoRefreshCookie(viewData, auto_start);
        return auto_start;
      }
      return cookie === COOKIE_TRUE;
    }
    return viewData.settings.auto_start_refresh;
  }

  /**
   * The ViewData struct definition.
   * @typedef {Object} ViewData
   * @property {string} name - The name of the view.
   * @property {string} display - The display ID of the view.
   * @property {Object} element - The DOM element of the view.
   * @property {Object} $element - The jQuery element object.
   * @property {Object} $container - The container of the jQuery $element object.
   * @property {array} settings - The settings of the view.
   * @property {string} cookieName - The name of the cookie used to store auto-refresh state.
   * @property {string} selector - The selector for the view based on its name and display ID.
   * @property {string} suffix - The suffix for unique identifiers related to the view.
   * @property {number|null} timer - The timer ID for auto-refreshing
   */

  /**
   * Build a ViewData object for a specific view.
   *
   * @param {string} view_name - The name of the view.
   * @param {string} view_display - The display ID of the view.
   * @param {array} view - The settings of the view.
   * @param {Object} viewElement - The DOM element of the view.
   * @return {ViewData} - The viewData object.
   */
  function initViewData(view_name, view_display, view, viewElement) {
    return {
      name: view_name,
      display: view_display,
      element: viewElement,
      $element: $(viewElement),
      $container: $(viewElement).parent(),
      settings: view,
      // Define the cookie name.
      cookieName: getCookieName(view_name, view_display),
      // Create a selector for the view based on its name and display ID.
      selector: getViewSelector(view_name, view_display),
      suffix: `${view_name}-${view_display}`,
      timer: null,
    };
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   * @return {boolean} - True if focus is in view content.
   */
  function hasFocus(viewData) {
    return viewData.$container.data(DATA_HAS_FOCUS) === true;
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   * @param {boolean} state - Set to true if focus is in view, false otherwise.
   */
  function setFocus(viewData, state) {
    viewData.$container.data(DATA_HAS_FOCUS, state);
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   * @return {boolean} - True if we are on the first page.
   */
  function isOnFirstPage(viewData) {
    return viewData.settings.current_page === 0;
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   * @return {string} - The focused element id.
   */
  function getFocusedElementId(viewData) {
    return viewData.$container.data(DATA_FOCUSED_ELEMENT_ID);
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   * @param {string} id - The focused element id.
   */
  function setFocusedElementId(viewData, id) {
    viewData.$container.data(DATA_FOCUSED_ELEMENT_ID, id);
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   */
  function unsetFocusedElementId(viewData) {
    viewData.$container.data(DATA_FOCUSED_ELEMENT_ID, null);
  }

  /**
   * Add the focus/blur listeners to the specified element.
   *
   * @param {ViewData} viewData - The viewData object.
   * @param {Object} $element - The jQuery element object.
   */
  function addFocusListeners(viewData, $element) {
    $element.on('focus', function () {
      setFocusedElementId(viewData, this.id);
    });
    $element.on('blur', function () {
      unsetFocusedElementId(viewData);
    });
  }

  /**
   * Clear the auto-refresh timer.
   *
   * @param {ViewData} viewData - The viewData object.
   */
  function clearTimer(viewData) {
    if (viewData.timer) {
      clearTimeout(viewData.timer);
      viewData.timer = null;
    }
  }

  /**
   * Enable auto-refreshing behaviour.
   *
   * @param {ViewData} viewData - The viewData object.
   */
  function enableAutoRefresh(viewData) {
    // Clear any existing timer.
    clearTimer(viewData);

    if (viewData.element) {
      // Set up the timer for the next refresh.
      viewData.timer = setTimeout(function () {
        Drupal.behaviors.views_auto_refresh.refresh(viewData.element);
      }, viewData.settings.interval);
    }

    // Update the button to indicate that auto-refresh is enabled.
    viewData.$element
      .find('.views-auto-refresh-button')
      .html(viewData.settings.auto_refresh_button_disable_label)
      .attr('aria-pressed', 'true')
      .addClass('refreshing')
      .removeClass('not-refreshing');
  }

  /**
   * Disable auto-refreshing behaviour.
   *
   * @param {ViewData} viewData - The viewData object.
   */
  function disableAutoRefresh(viewData) {
    // Clear any existing timer.
    clearTimer(viewData);

    // Update the button to indicate that auto-refresh is disabled.
    viewData.$element
      .find('.views-auto-refresh-button')
      .html(viewData.settings.auto_refresh_button_enable_label)
      .attr('aria-pressed', 'false')
      .addClass('not-refreshing')
      .removeClass('refreshing');
  }

  /**
   * Refresh the view one time.
   *
   * @param {Object} view - The view object containing settings and timer.
   * @param {string} execution_setting - The selector for the view element.
   */
  function refreshView(view, execution_setting) {
    // Function to refresh the view one time.
    Drupal.behaviors.views_auto_refresh.refresh(execution_setting);
  }

  function isFocusingIn(event, viewContent) {
    return !viewContent.contains(event.relatedTarget);
  }

  function isFocusingOut(event, viewContent) {
    return (
      event.relatedTarget === null || !viewContent.contains(event.relatedTarget)
    );
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   * @return {Object} - The focusable element if found.
   */
  function getFirstFocusableElement(viewData) {
    return viewData.$container.find('button, a').first();
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   */
  function restoreFocus(viewData) {
    // Restore focus on previously focused element when view is reloaded and
    // DOM constructed.
    setTimeout(() => {
      // Get the ID of the previously focused element.
      const focusedElementId = getFocusedElementId(viewData);
      if (focusedElementId) {
        // Unset the previously focused element.
        unsetFocusedElementId(viewData);
        let focusedElement;
        if (focusedElementId === FOCUS_DEFAULT_ID) {
          focusedElement = getFirstFocusableElement(viewData);
        } else {
          focusedElement = viewData.$container.find(`#${focusedElementId}`);
          if (focusedElement.length === 0) {
            focusedElement = getFirstFocusableElement(viewData);
          }
        }
        if (focusedElement) {
          focusedElement.focus();
        }
      }
    }, 0); // Adjust the timeout as necessary
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   * @return {boolean} - True if we can enable auto-refresh on this view page.
   */
  function isValidPageForAutoRefresh(viewData) {
    // Handle auto-refreshing stop on pagination.
    return isOnFirstPage(viewData) || !viewData.settings.stop_on_pagination;
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   */
  function initializeAutoRefresh(viewData) {
    // Check if the auto-refreshing behaviour when the page loads is enabled.
    if (isAutoRefreshEnabled(viewData)) {
      if (isValidPageForAutoRefresh(viewData)) {
        enableAutoRefresh(viewData);
      } else {
        disableAutoRefresh(viewData);
      }
    } else {
      disableAutoRefresh(viewData);
    }
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   * @return {boolean} - True if currently auto-refreshing based on timer value.
   */
  function isRefreshing(viewData) {
    return viewData.timer !== null;
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   */
  function managePauseOnFocus(viewData) {
    // Stop auto-refreshing when navigating on the view with tab key.
    // Restart when out of the view with tabulation key or click
    viewData.$element
      .find('.view-content')
      .attr({
        'aria-live': 'polite',
        'aria-atomic': 'true',
      })
      .on('focusin', function (event) {
        if (isFocusingIn(event, this)) {
          setFocus(viewData, true);
        }
      })
      .on('focusout', function (event) {
        if (isFocusingOut(event, this)) {
          setFocus(viewData, false);
          if (!isRefreshing(viewData)) {
            initializeAutoRefresh(viewData);
          }
        }
      })
      .on('keyup', function (event) {
        if (event.key === 'Tab' || event.key === 'Enter') {
          if (hasFocus(viewData) && isRefreshing(viewData)) {
            disableAutoRefresh(viewData);
          }
        }
      });
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   * @param {string} name - The button name.
   * @param {Object} $button - The jQuery Button element object.
   */
  function generateButtonIdIfMissing(viewData, name, $button) {
    if (!$button.attr('id')) {
      let buttonId = `${PREFIX}-${name}-${viewData.suffix}`;
      // Handle potential button id collision if displayed multiple times.
      // Buttons can be displayed in the header and/or footer areas of the view.
      if (document.getElementById(buttonId)) {
        let newButtonId;
        let j = 2;
        do {
          newButtonId = `${buttonId}-${j}`;
          j += 1;
        } while (document.getElementById(newButtonId));
        buttonId = newButtonId;
      }
      $button.attr('id', buttonId);
    }
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   */
  function manageToggleButton(viewData) {
    viewData.$element.find('.views-auto-refresh-button').each(function () {
      const $toggleButton = $(this);
      // Generate a unique ID for the button if it doesn't have one to place
      // focus back on refresh.
      generateButtonIdIfMissing(viewData, 'toggle', $toggleButton);

      // Attach click handler to enable/disable refreshing button.
      $toggleButton.on('click', function () {
        // Check refreshing status.
        if (isRefreshing(viewData)) {
          // Disable refreshing.
          disableAutoRefresh(viewData);
          setAutoRefreshCookie(viewData, false);
        } else {
          // Enable refreshing.
          enableAutoRefresh(viewData);
          setAutoRefreshCookie(viewData, true);
        }
      });

      // Store focused button ID for re-focusing after refresh.
      addFocusListeners(viewData, $toggleButton);
    });
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   */
  function manageRefreshNowButton(viewData) {
    // Handle the one-shot refresh button click behaviour.
    viewData.$element
      .find('.views-auto-refresh-refresh-now-button')
      .each(function () {
        const $refreshNowButton = $(this);

        // Set button label. Needed if button is in secondary area.
        $refreshNowButton.html(viewData.settings.refresh_now_button_label);

        // Generate a unique ID for the button if it doesn't have one to place
        // focus back on refresh.
        generateButtonIdIfMissing(viewData, 'refresh-now', $refreshNowButton);

        // Attach click handler to one-shot refresh button.
        $refreshNowButton.on('click', function () {
          // Refresh the view one time.
          refreshView(viewData.settings, viewData.selector);
        });

        // Store focused button ID for re-focusing after refresh.
        addFocusListeners(viewData, $refreshNowButton);
      });
  }

  /**
   * Generate a unique ID for a link.
   *
   * @param {ViewData} viewData - The viewData object.
   * @param {Object} $link - The jQuery $link element.
   */
  function generateLinkIdIfMissing(viewData, $link) {
    if (!$link.attr('id')) {
      // Sanitize the href value to be used in the ID
      const hrefValue = $link.attr('href').replace(/[^a-zA-Z0-9]/g, '-');
      $link.attr('id', `${PREFIX}-link-${viewData.suffix}-${hrefValue}`);
    }
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   */
  function manageViewContentLinks(viewData) {
    // Handle the pagination navigation focus behaviour.
    viewData.$element.find('.view-content a').each(function () {
      const $contentLink = $(this);
      // Generate a unique ID for each link if it doesn't have one to be able to
      // set focus back after refresh.
      generateLinkIdIfMissing(viewData, $contentLink);
      // Store focused button ID for re-focusing after refresh.
      addFocusListeners(viewData, $contentLink);
    });
  }

  /**
   * @param {ViewData} viewData - The viewData object.
   */
  function managePagerLinks(viewData) {
    // Handle the pagination navigation focus behaviour.
    viewData.$element.find('.pager a').each(function () {
      const $pagerLink = $(this);
      // Generate a unique ID for each link if it doesn't have one to be able to
      // set focus back after refresh.
      generateLinkIdIfMissing(viewData, $pagerLink);
      // Store focused button ID for re-focusing after refresh.
      addFocusListeners(viewData, $pagerLink);
      // Set focused element id to 'default' on pager link click.
      // We don't want to scroll back down to the pager after a page update.
      $pagerLink.on('click', function () {
        setFocusedElementId(viewData, FOCUS_DEFAULT_ID);
      });
    });
  }

  function getViewHandler(view_name, view_display, view) {
    return function viewHandler(viewElement) {
      const viewData = initViewData(view_name, view_display, view, viewElement);

      // Initialize the auto-refreshing behaviour on page load and view refresh.
      initializeAutoRefresh(viewData);

      // Restore focus after refresh for better accessibility.
      if (viewData.settings.restore_focus_after_refresh) {
        // Restore focus on the previously focused element.
        restoreFocus(viewData);
        // Process the view content links for focus behavior handling.
        manageViewContentLinks(viewData);
        // Process the pager links for focus behavior handling.
        managePagerLinks(viewData);
      }

      // Find enable/disable auto-refreshing button and handle behaviour.
      if (viewData.settings.auto_refresh_toggle_button) {
        manageToggleButton(viewData);
      }

      // Handle the one-shot refresh button click behaviour.
      if (viewData.settings.refresh_now_button) {
        manageRefreshNowButton(viewData);
      }

      // Stop auto-refreshing when navigating on the view with tab key.
      if (viewData.settings.stop_on_focused_view_content) {
        managePauseOnFocus(viewData);
      }
    };
  }

  function getViewElementsFromContext(context, viewSelector) {
    if ($(context).is(viewSelector)) {
      return [context];
    }
    return context.querySelectorAll(viewSelector);
  }

  // Define the behavior for the views-auto-refresh functionality.
  Drupal.behaviors.views_auto_refresh = {
    attach(context, settings) {
      // Loop through each view that has views-auto-refresh settings.
      for (const view_name in settings.views_auto_refresh) {
        // Loop through each display within the view.
        for (const view_display in settings.views_auto_refresh[view_name]) {
          // Process the view elements if present in DOM.
          const viewSelector = getViewSelector(view_name, view_display);
          const elements = getViewElementsFromContext(context, viewSelector);
          if (elements.length > 0) {
            const view = settings.views_auto_refresh[view_name][view_display];
            const viewHandler = getViewHandler(view_name, view_display, view);
            once('views-auto-refresh', elements).forEach(viewHandler);
          }
        }
      }
    },
    refresh(execution_setting) {
      $(execution_setting).trigger('RefreshView');
    },
  };
})(jQuery, Drupal, drupalSettings, once, window.Cookies);
