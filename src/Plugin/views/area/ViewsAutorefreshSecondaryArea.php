<?php

namespace Drupal\views_auto_refresh\Plugin\views\area;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;

/**
 * Defines an area plugin for the Auto Refresh secondary area.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("views_auto_refresh_secondary_area")
 */
class ViewsAutorefreshSecondaryArea extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {

    $options = parent::defineOptions();

    // Define option to enable or disable refreshing.
    $options['auto_refresh_toggle_button'] = ['default' => FALSE];
    // Define option to enable one shot refresh button.
    $options['refresh_now_button'] = ['default' => FALSE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    // Add a checkbox to enable or disable the auto-refresh button.
    $form['auto_refresh_toggle_button'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('Check this box to enable the "Enable/Disable the auto-refresh button" on the view.'),
      '#title' => $this->t('Enable/Disable the auto-refresh button'),
      '#default_value' => $this->options['auto_refresh_toggle_button'],
      '#required' => FALSE,
    ];

    // Add a checkbox to enable or disable one shot refresh button.
    $form['refresh_now_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable refresh now button.'),
      '#description' => $this->t('Check this box to enable a button that refreshes the view immediately.'),
      '#default_value' => $this->options['refresh_now_button'],
      '#required' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $view = $this->view;

    $auto_refresh_toggle_button = $this->options['auto_refresh_toggle_button'];
    $refresh_now_button = $this->options['refresh_now_button'];

    // Add enable or disable refreshing button.
    if ($auto_refresh_toggle_button) {
      $build['auto_refresh_toggle_button'] = [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#attributes' => [
          'class' => [
            'views-auto-refresh-button',
            $view->id() . '-' . $view->current_display,
          ],
          'type' => 'button',
          'aria-pressed' => 'false',
          'tabindex' => '0',
        ],
        '#value' => '',
      ];
    }

    // Add one shot refresh button.
    if ($refresh_now_button) {
      $build['refresh_now_button'] = [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#attributes' => [
          'class' => [
            'views-auto-refresh-refresh-now-button',
            $view->id() . '-' . $view->current_display,
          ],
          'type' => 'button',
          'tabindex' => '0',
        ],
        '#value' => '',
      ];
    }

    return $build;
  }

}
