<?php

namespace Drupal\views_auto_refresh\Plugin\views\area;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;

/**
 * Defines an area plugin for the Auto Refresh header.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("views_auto_refresh_area")
 */
class ViewsAutorefreshArea extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {

    $options = parent::defineOptions();
    // Define option for the refresh interval.
    $options['interval'] = ['default' => '50000'];
    // Define option to enable or disable refreshing.
    $options['auto_refresh_toggle_button'] = ['default' => FALSE];
    // Define option for the toggle refresh button label.
    $options['auto_refresh_button_enable_label'] = ['default' => 'Enable auto refresh'];
    // Define option for the toggle refresh button label.
    $options['auto_refresh_button_disable_label'] = ['default' => 'Disable auto refresh'];
    // Define option to enable one shot refresh button.
    $options['refresh_now_button'] = ['default' => FALSE];
    // Define option for the one shot refresh button label.
    $options['refresh_now_button_label'] = ['default' => 'Refresh now'];
    // Define option to enable auto start at page load.
    $options['auto_start_refresh'] = ['default' => FALSE];
    // Define option to restore focus after a view refresh for accessibility.
    $options['restore_focus_after_refresh'] = ['default' => FALSE];
    // Define option to stop refreshing if on a page of paginated view.
    $options['stop_on_pagination'] = ['default' => FALSE];
    // Define option to stop refreshing if the focus is on view-content.
    $options['stop_on_focused_view_content'] = ['default' => FALSE];
    // Define option to disable default Views Ajax scroll to top behavior.
    $options['disable_ajax_scroll_top'] = ['default' => FALSE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    // Add a text field for setting the interval in milliseconds.
    $form['interval'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Interval to check for new items (in milliseconds)'),
      '#description' => $this->t('Specify the interval at which the view should check for new items, in milliseconds.'),
      '#default_value' => $this->options['interval'],
      '#field_suffix' => 'milliseconds',
      '#required' => TRUE,
    ];

    // Add a checkbox to enable or disable the auto-refresh button.
    $form['auto_refresh_toggle_button'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('Check this box to enable the "Enable/Disable the auto-refresh button" on the view.'),
      '#title' => $this->t('Enable/Disable the auto-refresh button'),
      '#default_value' => $this->options['auto_refresh_toggle_button'],
      '#required' => FALSE,
    ];

    // Add a checkbox to enable or disable auto start.
    $form['auto_start_refresh'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable auto start.'),
      '#description' => $this->t('Check this box to start auto-refreshing behaviour when the page loads.'),
      '#default_value' => $this->options['auto_start_refresh'],
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="options[auto_refresh_toggle_button]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Add a text field for the label of toggle the auto-refresh button's label.
    $form['auto_refresh_button_enable_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Enable auto-refresh button's label"),
      '#description' => $this->t('Specify the label for the "Enable the auto-refresh button", span HTML tag is allowed.
      <br>WCAG: remember to define a visually-hidden/sr-only span label if you are using a CSS icon.
      <br>span > class="enable-auto-refresh-button-label visually-hidden sr-only"'),
      '#default_value' => $this->options['auto_refresh_button_enable_label'],
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="options[auto_refresh_toggle_button]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Add a text field for the label of toggle the auto-refresh button's label.
    $form['auto_refresh_button_disable_label'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Specify the label for the "Disable the auto-refresh button", span HTML tag is allowed.
      <br>WCAG: remember to define a visually-hidden/sr-only span label if you are using a CSS icon.
      <br>span > class="disable-auto-refresh-button-label visually-hidden sr-only"'),
      '#title' => $this->t("Disable auto-refresh button's label"),
      '#default_value' => $this->options['auto_refresh_button_disable_label'],
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="options[auto_refresh_toggle_button]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Add a checkbox to enable or disable one shot refresh button.
    $form['refresh_now_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable refresh now button.'),
      '#description' => $this->t('Check this box to enable a button that refreshes the view immediately.'),
      '#default_value' => $this->options['refresh_now_button'],
      '#required' => FALSE,
    ];

    // Add a text field for the label of the one shot refresh button.
    $form['refresh_now_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Specify the label for the "Refresh now button".'),
      '#description' => $this->t('Specify the label for the "Refresh now" button label, span HTML tag is allowed.
      <br>WCAG: remember to define a visually-hidden/sr-only span label if you are using a CSS icon.
      <br>span > class="refresh-now-button-label visually-hidden sr-only"'),
      '#default_value' => $this->options['refresh_now_button_label'],
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="options[refresh_now_button]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Add a checkbox to stop refreshing if on a page of paginated view.
    $form['stop_on_pagination'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Stop refreshing if not on the first page of a paginated view.'),
      '#description' => $this->t('Check this box to stop auto-refreshing behaviour when the view is on any page
      other than the first.'),
      '#default_value' => $this->options['stop_on_pagination'],
      '#required' => FALSE,
    ];

    // Add a checkbox for focus restoration after refresh for accessibility.
    $form['restore_focus_after_refresh'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Restore focus after refresh'),
      '#description' => $this->t('Check this box to try to restore focus after a view refresh for better accessibility.'),
      '#default_value' => $this->options['restore_focus_after_refresh'],
      '#required' => FALSE,
    ];

    // Add a checkbox to stop refreshing if focus is on view-content.
    $form['stop_on_focused_view_content'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Stop refreshing if focus is on view-content'),
      '#description' => $this->t('Check this box to stop auto-refreshing behaviour when the focus is on view-content while navigating with keyboard.'),
      '#default_value' => $this->options['stop_on_focused_view_content'],
      '#required' => FALSE,
    ];

    // Add a checkbox to disable default Views Ajax scroll to top behavior.
    $form['disable_ajax_scroll_top'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable "scroll to top" on refresh'),
      '#description' => $this->t('Check this box to disable the default Views Ajax scroll to top behavior after a refresh.'),
      '#default_value' => $this->options['disable_ajax_scroll_top'],
      '#required' => FALSE,
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $view = $this->view;
    $interval = $this->options['interval'];
    $auto_refresh_toggle_button = $this->options['auto_refresh_toggle_button'];
    // Ensure the values are strings.
    $auto_refresh_button_enable_label = is_string($this->options['auto_refresh_button_enable_label']) ? $this->options['auto_refresh_button_enable_label'] : '';
    $auto_refresh_button_disable_label = is_string($this->options['auto_refresh_button_disable_label']) ? $this->options['auto_refresh_button_disable_label'] : '';
    // Xss filter span tag to be able to replace button's label by a css icon.
    $auto_refresh_button_enable_label = Xss::filter($auto_refresh_button_enable_label, ['span']);
    $auto_refresh_button_disable_label = Xss::filter($auto_refresh_button_disable_label, ['span']);
    $refresh_now_button = $this->options['refresh_now_button'];
    // Ensure the values are strings.
    $refresh_now_button_label = is_string($this->options['refresh_now_button_label']) ? $this->options['refresh_now_button_label'] : '';
    // Xss filter span tag to be able to replace button's label by a css icon.
    $refresh_now_button_label = Xss::filter($refresh_now_button_label, ['span']);
    $auto_start_refresh = $this->options['auto_start_refresh'];
    $restore_focus_after_refresh = $this->options['restore_focus_after_refresh'];
    $stop_on_pagination = $this->options['stop_on_pagination'];
    $stop_on_focused_view_content = $this->options['stop_on_focused_view_content'];
    $disable_ajax_scroll_top = $this->options['disable_ajax_scroll_top'];

    // Attach the library and the settings.
    $build['#attached']['library'][] = 'views_auto_refresh/views_auto_refresh';
    $build['#attached']['drupalSettings']['views_auto_refresh'][$view->id()][$view->current_display] = [
      'interval' => $interval,
      'auto_refresh_toggle_button' => $auto_refresh_toggle_button,
      'auto_refresh_button_enable_label' => $auto_refresh_button_enable_label,
      'auto_refresh_button_disable_label' => $auto_refresh_button_disable_label,
      'refresh_now_button' => $refresh_now_button,
      'refresh_now_button_label' => $refresh_now_button_label,
      'auto_start_refresh' => $auto_refresh_toggle_button ? $auto_start_refresh : TRUE,
      'restore_focus_after_refresh' => $restore_focus_after_refresh,
      'stop_on_pagination' => $stop_on_pagination,
      'stop_on_focused_view_content' => $stop_on_focused_view_content,
      'disable_ajax_scroll_top' => $disable_ajax_scroll_top,
      'current_page' => $view->getCurrentPage(),
    ];

    // Add enable or disable refreshing button.
    if ($auto_refresh_toggle_button) {
      $build['auto_refresh_toggle_button'] = [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#attributes' => [
          'class' => [
            'views-auto-refresh-button',
            $view->id() . '-' . $view->current_display,
          ],
          'type' => 'button',
          'aria-pressed' => 'false',
          'tabindex' => '0',
        ],
        '#value' => $auto_refresh_button_enable_label,
      ];
    }

    // Add one shot refresh button.
    if ($refresh_now_button) {
      $build['refresh_now_button'] = [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#attributes' => [
          'class' => [
            'views-auto-refresh-refresh-now-button',
            $view->id() . '-' . $view->current_display,
          ],
          'type' => 'button',
          'tabindex' => '0',
        ],
        '#value' => $refresh_now_button_label,
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {

    // Call the parent class's validation method.
    parent::validateOptionsForm($form, $form_state);

    // Get values from the form state.
    $values = $form_state->getValue('options');
    $interval = $values['interval'];

    // Validate that the interval is a numeric value.
    if (!is_numeric($interval)) {
      $form_state->setError($form['interval'], $this->t('The interval has to be a numeric value.'));
    }
  }

}
