<?php

namespace Drupal\views_auto_refresh\EventSubscriber;

use Drupal\views\Ajax\ViewAjaxResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Response subscriber to handle AJAX responses.
 */
class AjaxResponseSubscriber implements EventSubscriberInterface {

  /**
   * Alter the views AJAX response commands.
   *
   * @param array $commands
   *   An array of commands to alter.
   */
  protected function removeScrollTopCommands(array &$commands) {
    foreach ($commands as $delta => &$command) {
      // Stop the view from scrolling to the top of the page.
      // We need to check for both commands as "viewsScrollTop" is deprecated
      // and not used in views_ajax.js for Drupal 10.1 anymore and replaced
      // by "scrollTop".
      if (in_array($command['command'], ['scrollTop', 'viewsScrollTop'])) {
        unset($commands[$delta]);
      }
    }
  }

  /**
   * Renders the ajax commands right before preparing the result.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event, which contains the possible AjaxResponse object.
   */
  public function onResponse(ResponseEvent $event) {
    $response = $event->getResponse();

    // Only alter views ajax responses.
    if (!($response instanceof ViewAjaxResponse)) {
      return;
    }

    // Keep scroll top behavior when changing page.
    if (!empty($event->getRequest()->get('page'))) {
      return;
    }

    $view = $response->getView();
    // No views_auto_refresh if header and footer are empty.
    if (empty($view->header) && empty($view->footer)) {
      return;
    }
    $disableScrollTop = FALSE;
    foreach ($view->header as $header) {
      if ($header->getPluginId() === 'views_auto_refresh_area') {
        $disableScrollTop = $header->options['disable_ajax_scroll_top'] ?? FALSE;
        break;
      }
    }
    if (!$disableScrollTop) {
      foreach ($view->footer as $footer) {
        if ($footer->getPluginId() === 'views_auto_refresh_area') {
          $disableScrollTop = $footer->options['disable_ajax_scroll_top'] ?? FALSE;
          break;
        }
      }
    }
    if (!$disableScrollTop) {
      return;
    }

    $commands = &$response->getCommands();
    $this->removeScrollTopCommands($commands);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::RESPONSE => [['onResponse']]];
  }

}
